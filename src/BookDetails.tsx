import React from 'react'
import {Grid} from '@material-ui/core'

import useArticles, {useArticlesInit} from './state/articles'

import ArticleList from './ArticleList'
import BookCover from './BookCover'
import BookTitle from './BookTitle'
import Progress from './Progress'
import Steps from './Steps'

export default function BookDetails() {
  useArticlesInit()

  const [{bookId}] = useArticles()

  return (
    <>
      <BookTitle />

      {bookId
        ? <>
            <Grid container spacing={2}>
             <Grid item xs={7}><Steps /></Grid>
             <Grid item xs={5}><BookCover /></Grid>
            </Grid>

            <ArticleList />
          </>
        : <Progress status="Loading articles..." />
      }
    </>
  )
}
