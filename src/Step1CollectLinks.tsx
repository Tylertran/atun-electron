import React from 'react'
import {makeStyles} from '@material-ui/core'

import ArticleListCollect from './ArticleListCollect'
import BookDelete from './BookDelete'

const useStyles = makeStyles((theme) => ({
  span: {
    marginLeft: theme.spacing(4)
  }
}))

export default function Step1CollectLinks() {
  const classes = useStyles()

  return (
    <>
      <ArticleListCollect />
      <span className={classes.span} />
      <BookDelete />
    </>
  )
}
