import React from 'react'
import {Stepper, Step, StepContent, StepLabel} from '@material-ui/core'

import Step1CollectLinks from './Step1CollectLinks'
import Step2GenerateBook from './Step2GenerateBook'

export default function Steps() {
  // User can rerun (update) any step
  return (
    <Stepper orientation="vertical">
      <Step active={true} completed={false} disabled={false} expanded={true}>
        <StepLabel>Collect list of articles in the Facebook page</StepLabel>
        <StepContent>
          <Step1CollectLinks />
        </StepContent>
      </Step>

      <Step active={true} completed={false} disabled={false} expanded={true}>
        <StepLabel>Generate book</StepLabel>
        <StepContent>
          <Step2GenerateBook />
        </StepContent>
      </Step>
    </Stepper>
  )
}
