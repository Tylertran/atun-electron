import React, {ChangeEvent, FormEvent, useState} from 'react'
import {Button, TextField} from '@material-ui/core'

import useArticles from './state/articles'
import useBooks from './state/books'
import useFbNav, {getFbTitle} from './state/fb'

import CopyableLink from './CopyableLink'

export default function AddBook() {
  const [, {addBook}] = useBooks()
  const [{url}] = useFbNav()
  const [, {initArticles}] = useArticles()

  const [title, setTitle] = useState(getFbTitle())
  const [titleInvalid, setTitleInvalid] = useState(false)

  const handleTitleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value)
    setTitleInvalid(false)
  }

  const handleAdd = async (event: FormEvent) => {
    event.preventDefault()

    const trimedTitle = title.trim()
    if (trimedTitle.length === 0) {
      setTitleInvalid(true)
    } else {
      const id = await addBook(url, trimedTitle)
      initArticles(id, [])
    }
  }

  return (
    <form onSubmit={handleAdd}>
      <h2>Add a new book</h2>

      <p>
        <b>Facebook page:</b>{' '}
        <CopyableLink url={url} />
      </p>

      <TextField
        label="Please enter the book title"
        fullWidth
        value={title}
        autoFocus
        onChange={handleTitleChange}
        error={titleInvalid}
      />

      <p><Button variant="contained" onClick={handleAdd}>Add</Button></p>
    </form>
  )
}
