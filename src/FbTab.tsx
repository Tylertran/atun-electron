import React from 'react'
import {IconButton, Toolbar} from '@material-ui/core'
import {ArrowBack, ArrowForward, Home} from '@material-ui/icons'
import {Alert} from '@material-ui/lab'

import useFbNav from './state/fb'
import CopyableLink from './CopyableLink'

export default function FbTab() {
  const [{onLine, url}, {goBack, goForward, goHome}] = useFbNav()

  return (
    <>
      <Toolbar id="fb-nav">
        <IconButton onClick={goBack} disabled={!onLine}>
          <ArrowBack />
        </IconButton>

        <IconButton onClick={goForward} disabled={!onLine}>
          <ArrowForward />
        </IconButton>

        <IconButton onClick={goHome} disabled={!onLine}>
          <Home />
        </IconButton>

        <CopyableLink url={url} />
      </Toolbar>

      {!onLine &&
        <Alert severity="warning" variant="filled">Could not connect to the Internet</Alert>
      }
    </>
  )
}
