import React from 'react'
import {Button} from '@material-ui/core'

import useArticles from './state/articles'
import ProgressDlg from './ProgressDlg'

const status = (articles: any[]) => `${articles.length} articles collected`

export default function ArticleListCollect() {
  const [
    {articles, fetchingLinks},
    {startFetchArticleLinks, requestFetchArticleLinksCancel}
  ] = useArticles()

  return (
    <>
      {articles!.length > 0 && <p>{status(articles!)}</p>}

      <Button
        variant="contained"
        onClick={() => startFetchArticleLinks()}
      >
        {articles!.length > 0 ? 'Collect new articles' : 'Collect'}
      </Button>

      <ProgressDlg
        open={fetchingLinks}
        title="Collecting articles..."
        status={status(articles!)}
        onCancel={requestFetchArticleLinksCancel}
      />
    </>
  )
}
