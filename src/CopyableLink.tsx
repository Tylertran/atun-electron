import React from 'react'
import {Link} from '@material-ui/core'

import {Clipboard} from 'electron'

import useNotification from './state/notification'

// @ts-ignore
const clipboard: Clipboard = window.electron.clipboard

type CopyableLinkProps = {
  url: string,
  title?: string
}

export default function CopyableLink({url, title}: CopyableLinkProps) {
  const [, {notify}] = useNotification()

  const copyLink = (event: Event, url: string) => {
    event.preventDefault()
    clipboard.writeText(url)
    notify('Link copied to clipboard')
  }

  return <Link href={url} onClick={(event: any) => copyLink(event, url)}>{title || url}</Link>
}
