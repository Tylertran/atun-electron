import React from 'react'
import {Snackbar, SnackbarContent} from '@material-ui/core'
import {withStyles} from '@material-ui/core/styles'

import useNotification from './state/notification'

const centerMessage = {
  message: {
    width: '100%',
    textAlign: 'center' as const  // https://github.com/typestyle/typestyle/issues/281
  }
}

export default function Notification() {
  const [{message, open}, {close}] = useNotification()
  const CenterSnackbarContent = withStyles(centerMessage)(SnackbarContent)

  return (
    <Snackbar
      open={open}
      autoHideDuration={2000}
      onClose={close}
      anchorOrigin={{vertical: 'top', horizontal: 'center'}}
      message={message}
    >
      <CenterSnackbarContent message={message} />
    </Snackbar>
  )
}
