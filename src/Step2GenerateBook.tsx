import React, {useState} from 'react'
import {Button, MenuItem, Select, makeStyles} from '@material-ui/core'

import useArticles from './state/articles'
import ProgressDlg from './ProgressDlg'

const PAGE_SIZES = ['A3', 'A4', 'A5', 'Legal', 'Letter', 'Tabloid']

const useStyles = makeStyles((theme) => ({
  generatePdf: {
    marginLeft: theme.spacing(1)
  }
}))

function selectedStatus(numArticles: number, numSelected: number) {
  const status = numSelected === 0
    ? 'Please select articles to include in the book'
    : `${numSelected}/${numArticles} articles selected to include in the book`
  return <p>{status}</p>
}

function bodiedStatus(numSelected: number, numBodied: number): string {
  return `${numBodied} / ${numSelected} content bodies of selected articles collected`
}

export default function Step2GenerateBook() {
  const [
    {articles, fetchingLinks, exporting},
    {startPdfExport, requestExportCancel}
  ] = useArticles()

  const classes = useStyles()
  const numArticles = articles.length

  const [pageSize, setPageSize] = useState('A4')

  const numSelected = articles.reduce((count, article) =>
    article.selected ? count + 1 : count
  , 0)

  const numBodied = articles.reduce((count, article) =>
    article.selected && article.bodied ? count + 1 : count
  , 0)

  const handlePageSizeChange = (event: any) => {
    setPageSize(event.target.value)
  }

  return (
    <>
      {numArticles > 0 && selectedStatus(numArticles, numSelected)}

      <Select
        value={pageSize}
        onChange={handlePageSizeChange}
      >
        {PAGE_SIZES.map(s => <MenuItem key={s} value={s}>{s}</MenuItem>)}
      </Select>

      <Button
        className={classes.generatePdf}
        variant="contained"
        disabled={numSelected === 0 || fetchingLinks || exporting}
        onClick={() => startPdfExport(pageSize)}
      >
        Generate PDF book
      </Button>

      <ProgressDlg
        open={exporting}
        title="Generating PDF book..."
        status={bodiedStatus(numSelected, numBodied)}
        onCancel={requestExportCancel}
      />
    </>
  )
}
