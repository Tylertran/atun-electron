import React, {ChangeEvent} from 'react'
import {Checkbox, Table, TableBody, TableCell, TableHead, TableRow} from '@material-ui/core'
import {withStyles} from '@material-ui/core/styles'
import _ from 'lodash'

import useArticles from './state/articles'

import ArticleBodyStatus from './ArticleBodyStatus'
import ArticleTitle from './ArticleTitle'

// Make rows denser so that users can scroll through the articles faster
// https://stackoverflow.com/questions/57325232/how-to-remove-lines-between-cells-in-table-in-material-ui
const noBorderTableCell = {
  root: {
    borderBottom: 'none',
    margin: 0,
    paddingTop: 0,
    paddingBottom: 0
  }
}

export default function ArticleList() {
  const NBTC = withStyles(noBorderTableCell)(TableCell)

  const [{articles, newArticles, fetchingLinks, exporting}, {selectAllArticles}] = useArticles()

  const allArticles = [...newArticles, ...articles]

  const numArticles = allArticles.length
  if (numArticles === 0) return null

  const numSelected = allArticles.reduce((count, article) =>
    article.selected ? count + 1 : count,
  0)

  const handleSelectAll = (event: ChangeEvent<HTMLInputElement>) => {
    selectAllArticles(event.target.checked)
  }

  return (
    <Table size="small" stickyHeader>
      <TableHead>
        <TableRow>
          <TableCell>
            <Checkbox
              color="primary"
              indeterminate={numSelected > 0 && numSelected < numArticles}
              checked={numArticles > 0 && numSelected === numArticles}
              disabled={fetchingLinks || exporting}
              onChange={handleSelectAll}
            />
            Article title (select to add to book)
          </TableCell>
          <TableCell>Date</TableCell>
          <TableCell align="center">Body</TableCell>
        </TableRow>
      </TableHead>

      <TableBody>
        {allArticles.map(({url, title, date, bodied, downloading, selected}, idx) => (
          <TableRow key={url} hover>
            <NBTC>
              <ArticleTitle
                numArticles={numArticles}
                idx={idx}
                url={url}
                // https://stackoverflow.com/questions/5796718/html-entity-decode
                title={_.unescape(title)}
                selected={selected}
                disabled={fetchingLinks || exporting}
              />
            </NBTC>
            <NBTC>{date}</NBTC>
            <NBTC align="center">
              <ArticleBodyStatus bodied={bodied} downloading={downloading} />
            </NBTC>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )
}
