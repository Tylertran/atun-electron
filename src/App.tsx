import React from 'react'
import {AppBar, CssBaseline, Tab, Tabs, makeStyles, useMediaQuery} from '@material-ui/core'
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles'

import {useArticlesListeners} from './state/articles'
import {useBooksInit} from './state/books'
import {useFbNavListener} from './state/fb'
import useTabs from './state/tabs'

import BooksTab from './BooksTab'
import FbTab from './FbTab'
import Notification from './Notification'

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  }
}))

type TabPanelProps = {
  children: any,
  value: number,
  index: number
}
const TabPanel = ({children, value, index}: TabPanelProps) => {
  return value === index ? children : null
}

export default function App() {
  useFbNavListener()
  useBooksInit()
  useArticlesListeners()

  // https://material-ui.com/customization/palette/
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')
  const theme = React.useMemo(() =>
    createMuiTheme({
      palette: {
        type: prefersDarkMode ? 'dark' : 'light'
      }
    }),
    [prefersDarkMode]
  )

  const classes = useStyles()

  const [{selectedTabIdx}, {selectTab}] = useTabs()

  const onTabChange = (event: any, tabIdx: number) => {
    selectTab(tabIdx)
  }

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />

      <AppBar position="fixed" className={classes.appBar}>
        <Tabs value={selectedTabIdx} onChange={onTabChange}>
          <Tab label="Facebook" />
          <Tab label="Books" />
        </Tabs>
      </AppBar>

      {/* AppBar position is "fixed", its position is absolute and it's on top of other elements */}
      <div className="fixed-app-bar-spacer"></div>

      <TabPanel value={selectedTabIdx} index={0}>
        <FbTab />
      </TabPanel>

      <TabPanel value={selectedTabIdx} index={1}>
        <BooksTab />
      </TabPanel>

      <Notification />
    </ThemeProvider>
  )
}
