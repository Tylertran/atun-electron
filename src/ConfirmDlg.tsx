import React from 'react'
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from '@material-ui/core'

type ConfirmDlgProps = {
  open: boolean
  question: string
  onConfirm: (confirmed: boolean) => void
}

export default function ConfirmDlg({open, question, onConfirm}: ConfirmDlgProps) {
  return (
    <Dialog open={open}>
      <DialogTitle>Confirm</DialogTitle>
      <DialogContent>
        <DialogContentText>{question}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button variant="contained" color="secondary" onClick={() => onConfirm(true)}>
          OK
        </Button>
        <Button variant="contained" autoFocus onClick={() => onConfirm(false)}>
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  )
}
