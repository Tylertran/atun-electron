import React from 'react'
import {Box, Icon} from '@material-ui/core'
import {Check} from '@material-ui/icons'

import Progress from './Progress'

type ArticleBodyStatusProps = {
  bodied: boolean,
  downloading: boolean
}

export default function ArticleBodyStatus({bodied, downloading}: ArticleBodyStatusProps) {
  if (downloading) return (
    <Box display="flex" justifyContent="center" alignItems="center">
      <Progress />
    </Box>
  )

  if (bodied) return (
    <Icon color="primary"><Check /></Icon>
  )

  return null
}
