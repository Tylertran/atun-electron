import React from 'react'

import useBooks from './state/books'

import AddBook from './AddBook'
import BookDetails from './BookDetails'
import BookList from './BookList'

export default function BooksTab() {
  const [{selectedId}] = useBooks()

  return (
    <>
      <BookList />

      <div className="book-list-spacer">
        {selectedId ? <BookDetails /> : <AddBook />}
      </div>
    </>
  )
}
