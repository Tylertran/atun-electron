import React from 'react'
import {Checkbox} from '@material-ui/core'

import useArticles from './state/articles'
import CopyableLink from './CopyableLink'

const FB_DOMAIN = 'https://www.facebook.com'

const urlWithDomain = (url: string) =>
  url.startsWith('/') ? FB_DOMAIN + url : url

type ArticleTitleProps = {
  numArticles: number
  idx: number
  url: string
  title: string
  selected: boolean
  disabled: boolean
}

export default function ArticleTitle({numArticles, idx, url, title, selected, disabled}: ArticleTitleProps) {
  const [, {selectArticle}] = useArticles()

  const handleSelect = (event: any, idx: number) => {
    selectArticle(idx, event.target.checked)
  }

  return (
    <>
      <Checkbox
        color="primary"
        checked={selected}
        disabled={disabled}
        onClick={(event) => handleSelect(event, idx)}
      />
      {numArticles > 1 && `${idx + 1}. `}
      <CopyableLink url={urlWithDomain(url)} title={title} />
    </>
  )
}
