import React, {useState} from 'react'
import {Button} from '@material-ui/core'

import useArticles from './state/articles'
import useBooks from './state/books'

import ConfirmDlg from './ConfirmDlg'

export default function BookDelete() {
  const [{fetchingLinks, exporting}] = useArticles()
  const [, {deleteSelectedBook}] = useBooks()

  const [deleteDlgOpen, setDeleteDlgOpen] = useState(false)

  const handleDeleteDlg = (confirmed: boolean) => {
    setDeleteDlgOpen(false)
    if (confirmed) {
      deleteSelectedBook()
    }
  }

  return (
    <>
      <Button
        variant="contained"
        color="secondary"
        disabled={fetchingLinks || exporting}
        onClick={() => setDeleteDlgOpen(true)}
      >
        Delete
      </Button>

      <ConfirmDlg
        open={deleteDlgOpen}
        question="Do you want to delete collected articles?"
        onConfirm={handleDeleteDlg}
      />
    </>
  )
}
