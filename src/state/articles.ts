import {useEffect} from 'react'
import {StoreActionApi, createStore, createHook} from 'react-sweet-state'

import {IpcRenderer} from 'electron'

import {useSelectedBook} from './books'
import useNotification from './notification'

// @ts-ignore
const ipcRenderer: IpcRenderer = window.electron.ipcRenderer

type Link = {
  url: string
  title: string
  date: string
}

// Apart from url, title, date, and bodied properties from Electron side,
// we add selected and downloading properties.
type Article = Link & {
  bodied: boolean
}

type ArticleState = Article & {
  selected: boolean,
  downloading: boolean
}

type State = {
  bookId: string,

  articles: ArticleState[],
  newArticles: ArticleState[],

  fetchingLinks: boolean,
  fetchingLinksCancelToken: string,

  exporting: boolean,
  exportingCancelToken: string
}

type StoreApi = StoreActionApi<State>

const initialState: State = {
  bookId: '',

  articles: [],
  newArticles: [],

  fetchingLinks: false,
  fetchingLinksCancelToken: '',

  exporting: false,
  exportingCancelToken: ''
}

const actions = {
  initArticles: (bookId: string, articles: Article[]) => ({setState}: StoreApi) => {
    setState({
      bookId,
      articles: articles.map(a => ({...a, selected: false, downloading: false})),
      newArticles: [],
      fetchingLinks: false,
      fetchingLinksCancelToken: undefined,
      exporting: false,
      exportingCancelToken: undefined
    })
  },

  selectArticle: (idx: number, selected: boolean) => ({getState, setState}: StoreApi) => {
    const {articles} = getState()
    const article = articles![idx]
    article.selected = selected
    setState({articles})
  },

  selectAllArticles: (selected: boolean) => ({getState, setState}: StoreApi) => {
    const {articles} = getState()
    articles!.forEach(a => a.selected = selected)
    setState({articles})
  },

  startFetchArticleLinks: () => ({getState, setState}: StoreApi) => {
    const {bookId} = getState()
    const fetchingLinksCancelToken = ipcRenderer.sendSync('startFetchArticleLinksSync', bookId)
    if (fetchingLinksCancelToken) setState({fetchingLinks: true, fetchingLinksCancelToken})
  },

  onNewArticleLinks: (bookId: string, newLinks: Link[]) => ({getState, setState}: StoreApi) => {
    const {bookId: bid, newArticles} = getState()
    if (bookId !== bid) return

    newArticles!.push(...newLinks.map(l => ({...l, bodied: false, selected: false, downloading: false})))
    setState({newArticles})
  },

  onFetchArticleLinksFinish: (bookId: string) => ({getState, setState}: StoreApi) => {
    const {bookId: bid, articles, newArticles} = getState()
    if (bookId !== bid) return

    articles!.unshift(...newArticles!)
    setState({articles, newArticles: [], fetchingLinks: false, fetchingLinksCancelToken: undefined})
  },

  requestFetchArticleLinksCancel: () => ({getState}: StoreApi) => {
    const {fetchingLinksCancelToken} = getState()
    ipcRenderer.send('requestCancel', fetchingLinksCancelToken)
  },

  startPdfExport: (pageSize: string) => ({getState, setState}: StoreApi) => {
    const {bookId, articles} = getState()
    const idxs = articles!.flatMap(({selected}, idx) => selected ? [idx] : [])
    const exportingCancelToken = ipcRenderer.sendSync('startPdfExportSync', bookId, idxs, pageSize)
    if (exportingCancelToken) {
      setState({exporting: true, exportingCancelToken})
    }
  },

  onArticleBodyFetchStart: (bookId: string, articleUrl: string) => ({getState, setState}: StoreApi) => {
    const {bookId: bid, articles} = getState()
    if (bookId !== bid) return

    const idx = articles!.findIndex(a => a.url === articleUrl)
    if (idx < 0) return

    const article = articles![idx]
    article.downloading = true
    setState({articles})
  },

  onArticleBodyFetchFinish: (bookId: string, articleUrl: string, bodied: boolean) => ({getState, setState}: StoreApi) => {
    const {bookId: bid, articles} = getState()
    if (bookId !== bid) return

    const idx = articles!.findIndex(a => a.url === articleUrl)
    if (idx < 0) return

    const article = articles![idx]
    if (!article) return

    article.downloading = false
    article.bodied = bodied
    setState({articles})
  },

  onExportFinish: (bookId: string) => ({getState, setState}: StoreApi) => {
    const {bookId: bid} = getState()
    if (bookId !== bid) return

    setState({exporting: false, exportingCancelToken: undefined})
  },

  requestExportCancel: () => ({getState}: StoreApi) => {
    const {exportingCancelToken} = getState()
    ipcRenderer.send('requestCancel', exportingCancelToken)
  }
}

// Articles of the selected book (only one book); see BooksStore in books.js.
// To save memory, we don't load all articles of all books to memory.
const ArticlesStore = createStore({initialState, actions})

const useArticles = createHook(ArticlesStore)
export default useArticles

export function useArticlesInit() {
  const [book] = useSelectedBook()
  const [, {initArticles}] = useArticles()

  useEffect(() => {
    if (book) {
      ipcRenderer.invoke('loadArticles', book.id).then((articles: Article[]) =>
        initArticles(book.id, articles)
      )
    }
  }, [book, initArticles])
}

export function useArticlesListeners() {
  const [, {
    onNewArticleLinks, onFetchArticleLinksFinish, requestFetchArticleLinksCancel,
    onArticleBodyFetchStart, onArticleBodyFetchFinish,
    onExportFinish, requestExportCancel
  }] = useArticles()

  const [, {notify}] = useNotification()

  useEffect(() => {
    const newArticleLinksListener = (event: any, bookId: string, newLinks: Link[]) => {
      onNewArticleLinks(bookId, newLinks)
    }

    const onFetchArticleLinksFinishListener = (event: any, bookId: string) => {
      onFetchArticleLinksFinish(bookId)
    }

    const articleBodyFetchStartListener = (event: any, bookId: string, articleUrl: string) => {
      onArticleBodyFetchStart(bookId, articleUrl)
    }

    const articleBodyFetchFinishListener = (event: any, bookId: string, articleUrl: string, bodied: boolean) => {
      onArticleBodyFetchFinish(bookId, articleUrl, bodied)
    }

    const pdfExportFinishListener = (event: any, bookId: string, success: boolean) => {
      onExportFinish(bookId)
      notify(success ? 'Exported PDF file successfully' : 'Failed exporting PDF file')
    }

    ipcRenderer.on('newArticleLinks', newArticleLinksListener)
    ipcRenderer.on('fetchArticleLinksFinish', onFetchArticleLinksFinishListener)

    ipcRenderer.on('articleBodyFetchStart', articleBodyFetchStartListener)
    ipcRenderer.on('articleBodyFetchFinish', articleBodyFetchFinishListener)

    ipcRenderer.on('pdfExportFinish', pdfExportFinishListener)

    return () => {
      requestFetchArticleLinksCancel()
      requestExportCancel()

      ipcRenderer.removeListener('newArticleLinks', newArticleLinksListener)
      ipcRenderer.removeListener('fetchArticleLinksFinish', onFetchArticleLinksFinishListener)

      ipcRenderer.removeListener('articleBodyFetchStart', articleBodyFetchStartListener)
      ipcRenderer.removeListener('articleBodyFetchFinish', articleBodyFetchFinishListener)

      ipcRenderer.removeListener('pdfExportFinish', pdfExportFinishListener)
    }
  }, [
    onNewArticleLinks, onFetchArticleLinksFinish, requestFetchArticleLinksCancel,
    onArticleBodyFetchStart, onArticleBodyFetchFinish,
    onExportFinish, requestExportCancel, notify
  ])
}
