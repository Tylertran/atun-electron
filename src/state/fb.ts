import {useEffect} from 'react'
import {StoreActionApi, createStore, createHook} from 'react-sweet-state'

import {IpcRenderer} from 'electron'

// @ts-ignore
const ipcRenderer: IpcRenderer = window.electron.ipcRenderer

type State = {
  onLine: boolean
  url: string
}

type StoreApi = StoreActionApi<State>

const initialState: State = {
  onLine: navigator.onLine,  // https://www.electronjs.org/docs/tutorial/online-offline-events
  url: ipcRenderer.sendSync('getFbUrlSync')
}

export const getFbTitle = () => {
  return ipcRenderer.sendSync('getFbTitleSync')
}

const actions = {
  onOnLineChange: () => ({setState}: StoreApi) => {
    const {onLine} = navigator
    setState({onLine})
    ipcRenderer.send('onLineChange', onLine)
  },

  onUrlChange: (url: string) => ({setState}: StoreApi) => {
    setState({url})
  },

  goBack: () => () => {
    ipcRenderer.send('fbGoBack')
  },

  goForward: () => () => {
    ipcRenderer.send('fbGoForward')
  },

  goHome: () => () => {
    ipcRenderer.send('fbGoHome')
  }
}

const FbNavStore = createStore({initialState, actions})

const useFbNav = createHook(FbNavStore)
export default useFbNav

export const useFbNavListener = () => {
  const [, {onOnLineChange, onUrlChange}] = useFbNav()

  useEffect(() => {
    const urlChangeListener = (event: any, newUrl: string) => {
      onUrlChange(newUrl)
    }

    window.addEventListener('online',  onOnLineChange)
    window.addEventListener('offline',  onOnLineChange)
    ipcRenderer.on('fbUrlChange', urlChangeListener)

    return () => {
      window.removeEventListener('online',  onOnLineChange)
      window.removeEventListener('offline',  onOnLineChange)
      ipcRenderer.removeListener('fbUrlChange', urlChangeListener)
    }
  }, [onOnLineChange, onUrlChange])
}

ipcRenderer.send('onLineChange', navigator.onLine)
