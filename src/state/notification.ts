import {StoreActionApi, createStore, createHook} from 'react-sweet-state'

type State = {
  message: string,
  open: boolean
}

type StoreApi = StoreActionApi<State>

const initialState = {
  message: '',
  open: false
}

const actions = {
  notify: (message: string) => ({setState}: StoreApi) => {
    setState({message, open: true})
  },

  close: () => ({setState}: StoreApi) => {
    setState({open: false})
  }
}

const NotificationStore = createStore({initialState, actions})

const useNotification = createHook(NotificationStore)
export default useNotification
