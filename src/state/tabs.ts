import {createStore, createHook} from 'react-sweet-state'
import {IpcRenderer} from 'electron'

// @ts-ignore
const ipcRenderer: IpcRenderer = window.electron.ipcRenderer

const INITIAL_TAB_IDX = 1

const TabsStore = createStore({
  initialState: {
    selectedTabIdx: INITIAL_TAB_IDX
  },

  actions: {
    selectTab: (idx) => ({setState}) => {
      setState({selectedTabIdx: idx})
      ipcRenderer.send('tabChange', idx)
    }
  }
})

const useTabs = createHook(TabsStore)
export default useTabs

ipcRenderer.send('tabChange', INITIAL_TAB_IDX)
