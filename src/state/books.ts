import {useEffect} from 'react'
import {StoreActionApi, createStore, createHook} from 'react-sweet-state'

import {IpcRenderer} from 'electron'

// @ts-ignore
const ipcRenderer: IpcRenderer = window.electron.ipcRenderer

type Book = {
  id: string
  url: string
  title: string
}

type State = {
  loading: boolean
  books: Book[]
  selectedId: string
}

type StoreApi = StoreActionApi<State>

const initialState: State = {
  loading: true,
  books: [],
  selectedId: ''
}

const getSelectedBook = (state: State) => {
  const {books, selectedId} = state
  return books.find(b => b.id === selectedId)
}

const actions = {
  initBooks: (books: Book[]) => ({setState}: StoreApi) => {
    setState({books, selectedId: undefined})
  },

  addBook: (url: string, title: string) => async ({getState, setState}: StoreApi) => {
    const id = await ipcRenderer.invoke('addBook', url, title)

    const {books} = getState()
    books.unshift({id, url, title})
    setState({books, selectedId: id})

    return id
  },

  selectBook: (id?: string) => ({setState}: StoreApi) => {
    setState({selectedId: id})
  },

  setSelectedBookTitle: (title: string) => async ({getState, setState}: StoreApi) => {
    const state = getState()
    const {books, selectedId} = state
    await ipcRenderer.invoke('setBookTitle', selectedId, title)

    const book = getSelectedBook(state)
    if (book) {
      book.title = title
      setState({books})
    }
  },

  deleteSelectedBook: () => async ({getState, setState}: StoreApi) => {
    const {books, selectedId} = getState()
    await ipcRenderer.invoke('deleteBook', selectedId)

    const idx = books.findIndex(b => b.id === selectedId)
    books.splice(idx, 1)
    setState({books, selectedId: undefined})
  }
}

const BooksStore = createStore({initialState, actions})

const useBooks = createHook(BooksStore)
export default useBooks

export const useSelectedBook = createHook(BooksStore, {selector: getSelectedBook})

export const useBooksInit = () => {
  const [, {initBooks}] = useBooks()

  useEffect(() => {
    ipcRenderer.invoke('loadBooks').then(initBooks)
  }, [initBooks])
}
