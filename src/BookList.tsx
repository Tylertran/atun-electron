import React from 'react'
import {Divider, Drawer, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core'
import {MenuBookTwoTone} from '@material-ui/icons'

import useBooks from './state/books'
import Progress from './Progress'

export default function BookList() {
  const [{books, selectedId}, {selectBook}] = useBooks()

  if (books === null) return <Progress status="Loading books..." />

  return (
    <Drawer
      variant="permanent"
      anchor="left"
      classes={{paper: 'book-list'}}
    >
      <div className="fixed-app-bar-spacer"></div>

      <List>
        <ListItem
          selected={selectedId === null}
          button
          onClick={() => {
            if (selectedId !== null) selectBook(undefined)
          }}
        >
          <ListItemIcon>
            <MenuBookTwoTone />
          </ListItemIcon>

          <ListItemText primary="Add book" />
        </ListItem>
        <Divider />

        {books.map(({id, title}) =>
          <div key={id}>
            <ListItem
              selected={id === selectedId}
              button
              onClick={() => {
                if (id !== selectedId) selectBook(id)
              }}
            >
              {title}
            </ListItem>
            <Divider />
          </div>
        )}
      </List>
    </Drawer>
  )
}
