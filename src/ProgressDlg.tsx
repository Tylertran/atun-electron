import React from 'react'
import {Button, Dialog, DialogActions, DialogContent, DialogTitle} from '@material-ui/core'
import Progress from './Progress'

type ProgressDlgProps = {
  open: boolean
  title: string
  status: string
  onCancel: () => void
}

export default function ProgressDlg({open, title, status, onCancel}: ProgressDlgProps) {
  return (
    <Dialog open={open}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <Progress status={status} />
      </DialogContent>
      <DialogActions>
        <Button variant="contained" autoFocus onClick={onCancel}>
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  )
}
