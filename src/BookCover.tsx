import React, {useEffect, useState} from 'react'
import {Button} from '@material-ui/core'

import {IpcRenderer} from 'electron'
import useArticles from './state/articles'
import CopyableLink from './CopyableLink'

// @ts-ignore
const ipcRenderer: IpcRenderer = window.electron.ipcRenderer

export default function BookCover() {
  const [{bookId}] = useArticles()
  const [imgSrc, setImgSrc] = useState<string | null>(null)

  useEffect(() => {
    setImgSrc(ipcRenderer.sendSync('getBookCoverImageSync', bookId))
  }, [setImgSrc, bookId])

  return (
    <>
      {imgSrc &&
        <>
          <img alt="Cover" src={imgSrc} style={{maxWidth: '75%'}} />
          <br />
        </>
      }

      <Button
        variant="contained"
        onClick= {() => {
          const imgSrc = ipcRenderer.sendSync('setBookCoverImageSync', bookId)
          setImgSrc(imgSrc)
        }}
      >
        Select book cover image
      </Button>

      <p><CopyableLink url="https://www.postermywall.com/" title="PosterMyWall" /> is a good tool to create book cover.</p>
    </>
  )
}
