import React, {ChangeEvent, FormEvent, KeyboardEvent, useState} from 'react'
import {TextField} from '@material-ui/core'

import useBooks, {useSelectedBook} from './state/books'
import CopyableLink from './CopyableLink'

type BookTitleEditorProps = {
  title: string
  onDone: () => void
}

const BookTitleEditor = ({title: originalTitle, onDone}: BookTitleEditorProps) => {
  const [, {setSelectedBookTitle}] = useBooks()
  const [title, setTitle] = useState(originalTitle)
  const [titleInvalid, setTitleInvalid] = useState(false)

  const handleTitleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const title = event.target.value
    setTitle(title)

    const trimedTitle = title.trim()
    setTitleInvalid(trimedTitle.length === 0)
  }

  const handleEsc = (event: KeyboardEvent<HTMLDivElement>) => {
    if (event.keyCode === 27) onDone()
  }

  const handleTitleUpdateDone = async (event: FormEvent) => {
    event.preventDefault()
    if (!titleInvalid && title !== originalTitle) {
      setSelectedBookTitle(title.trim())
      onDone()
    }
  }

  return (
    <form onSubmit={handleTitleUpdateDone}>
      <TextField
        label="Please enter the book title"
        fullWidth
        value={title}
        autoFocus
        onChange={handleTitleChange}
        onBlur={onDone}
        onKeyDown={handleEsc}
        error={titleInvalid}
      />
    </form>
  )
}

export default function BookTitle() {
  const [book] = useSelectedBook()
  const [editing, setEditing] = useState(false)

  const {url, title} = book!

  return (
    <>
      {!editing && <h2 onClick={() => setEditing(true)}>{title}</h2>}

      {editing && <BookTitleEditor title={title} onDone={() => setEditing(false)} />}

      <p>
        <b>Facebook page:</b>{' '}
        <CopyableLink url={url} />
      </p>
    </>
  )
}
