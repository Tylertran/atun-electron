import React from 'react'
import {Box, CircularProgress, makeStyles} from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  status: {
    marginLeft: theme.spacing(1)
  }
}))

// https://material-ui.com/api/circular-progress/
const SIZE = 20
const THICKNESS = 1.8

export default function Progress({status = ''}) {
  const classes = useStyles()

  return (
    <Box display="flex" alignItems="center">
      <CircularProgress size={SIZE} thickness={THICKNESS} />
      <div className={classes.status}>{status}</div>
    </Box>
  )
}
