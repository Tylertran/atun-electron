import {PageMaker} from 'atun-core'
import {app, BrowserWindow} from 'electron'
import puppeteer, {Browser} from 'puppeteer-core'
import pie from 'puppeteer-in-electron'

import {SHOW_PUPPETEER} from './debug'

let browser: undefined | Browser = undefined

// Must be called at startup before the electron app is ready:
// https://github.com/TrevorSundberg/puppeteer-in-electron/blob/master/index.ts
export async function initPuppeteer() {
  await pie.initialize(app)
  browser = await pie.connect(app, puppeteer)
}

export function newPageMaker(window?: BrowserWindow): PageMaker {
  return async (url?: string) => {
    const w = window ? window : new BrowserWindow({show: SHOW_PUPPETEER})
    if (url) await w.loadURL(url)
    return await pie.getPage(browser!, w)
  }
}
