import {Article as PdfArticle, generateCoverPdf, generateArticlePdfs, generateTocPdf, mergePdfs} from 'atun-core'
import {BrowserWindow, dialog, ipcMain} from 'electron'
import fs from 'fs'
import {PDFFormat} from 'puppeteer-core'

import {fetchMissingArticleBodies} from './bodies'
import {submitTask} from './cancel'
import {getBookCoverImage} from './cover'
import {Article, articleBodied} from './db'
import {SHOW_PUPPETEER} from './debug'
import {newPageMaker} from './pie'

async function exportPdf(bookId: string, articles: Article[], pageSize: PDFFormat, finalPdfPath: string): Promise<boolean> {
  const window = new BrowserWindow({show: SHOW_PUPPETEER})
  const pageMaker = newPageMaker(window)

  const articlePdfPathsPrefix = `tmp/book-${bookId}-`
  const tocPdfPath = `tmp/book-${bookId}-toc.pdf`
  const coverPdfPath = `tmp/book-${bookId}-cover.pdf`

  const articlePdfPaths: string[] = []
  await generateArticlePdfs(articles as PdfArticle[], pageMaker, pageSize, async (articleIdx) => {
    const articlePdfData = await window.webContents.printToPDF({printBackground: true})
    const articlePdfPath = articlePdfPathsPrefix + articleIdx.toString().padStart(3, '0') + '.pdf'
    fs.writeFileSync(articlePdfPath, articlePdfData)
    articlePdfPaths.push(articlePdfPath)
  })

  await generateTocPdf(articles as PdfArticle[], pageMaker, pageSize, articlePdfPaths, true)
  const tocPdfData = await window.webContents.printToPDF({})
  fs.writeFileSync(tocPdfPath, tocPdfData)

  const coverImageDataUrl = getBookCoverImage(bookId)
  if (coverImageDataUrl) {
    await generateCoverPdf(coverImageDataUrl, pageMaker, pageSize)
    const coverPdfData = await window.webContents.printToPDF({marginsType: 1})
    fs.writeFileSync(coverPdfPath, coverPdfData)

    await mergePdfs([coverPdfPath, tocPdfPath, ...articlePdfPaths], finalPdfPath)
  } else {
    await mergePdfs([tocPdfPath, ...articlePdfPaths], finalPdfPath)
  }

  return true
}

export function initExportIpc() {
  ipcMain.on('startPdfExportSync', (event, bookId, idxs, pageSize) => {
    const finalPdfPath = dialog.showSaveDialogSync({
      title: 'Save PDF file',
      buttonLabel: 'Save',
      defaultPath: `${bookId}.pdf`,
      filters: [{name: 'PDF file', extensions: ['pdf']}
    ]})

    if (!finalPdfPath) {
      event.returnValue = ''
      return
    }

    const token = submitTask(async (cancelRequested) => {
      const onFetchStart = (article: Article) => {
        event.sender.send('articleBodyFetchStart', bookId, article.url)
      }
      const onFetchFinish = (article: Article) => {
        event.sender.send('articleBodyFetchFinish', bookId, article.url, articleBodied(article))
      }
      const selectedArticles = await fetchMissingArticleBodies(
        bookId, idxs, cancelRequested, onFetchStart, onFetchFinish
      )
      const success = await exportPdf(bookId, selectedArticles, pageSize, finalPdfPath)
      event.sender.send('pdfExportFinish', bookId, success)
    })

    event.returnValue = token
  })
}
