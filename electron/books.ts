import {ipcMain} from 'electron'
import {loadBooks, addBook, setBookTitle, deleteBook} from './db'

export function initBooksIpc() {
  ipcMain.handle('loadBooks', () => loadBooks())

  ipcMain.handle('addBook', (event, url, title) => addBook(url, title))

  ipcMain.handle('setBookTitle', (event, id, title) => setBookTitle(id, title))

  ipcMain.handle('deleteBook', (event, id) => deleteBook(id))
}
