import {ipcMain} from 'electron'
import {fetchStoryLinks} from 'atun-core'

import {submitTask} from './cancel'
import {articleBodied, loadArticles, loadBooks, storeArticles} from './db'
import {FB_BASE_URL} from './fb'
import {newPageMaker} from './pie'

export function initLinksIpc() {
  const pageMaker = newPageMaker()

  ipcMain.handle('loadArticles', (event, bookId) => {
    const articles = loadArticles(bookId)
    return articles.map(a => {
      const bodied = articleBodied(a)
      delete a.body
      return {...a, bodied}
    })
  })

  ipcMain.on('startFetchArticleLinksSync', (event, bookId) => {
    const books = loadBooks()
    const book = books.find(b => b.id === bookId)
    if (!book) {
      event.returnValue = undefined
      return
    }

    const bookUrl = book.url
    const path = bookUrl.substring(FB_BASE_URL.length)

    const token = submitTask(async (cancelRequested) => {
      // For collecting new articles of existing book:
      // if there are existing articles, collect until hitting an existing one
      const existingArticles = loadArticles(bookId)
      const existingUrls = existingArticles.map(({url}) => url)

      const newLinksAcc = []

      try {
        for await (const links of fetchStoryLinks(pageMaker, path, cancelRequested)) {
          const newLinks = links.filter(({url}) => !existingUrls.includes(url))
          console.log(`${newLinks.length}/${links.length} more articles collected`)

          if (newLinks.length > 0) {
            event.sender.send('newArticleLinks', bookId, newLinks)
            newLinksAcc.push(...newLinks)
          }

          if (newLinks.length !== links.length) break
        }
      } catch (e) {
        // Catch errors to keep the collected articles
        console.error(e)
      }

      // Prepend newLinksAcc to existingArticles
      if (newLinksAcc.length > 0) {
        existingArticles.unshift(...newLinksAcc)
        storeArticles(bookId, existingArticles)
      }

      event.sender.send('fetchArticleLinksFinish', bookId)
    })

    event.returnValue = token
  })
}
