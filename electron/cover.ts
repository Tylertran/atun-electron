import {getImageDataUrl} from 'atun-core'
import {dialog, ipcMain} from 'electron'
import fs from 'fs'
import path from 'path'

const COVER_IMG_EXTS = ['jpg', 'jpeg', 'png']

export function getCoverFilePath(bookId: string, ext: string): string {
  return `tmp/book-${bookId}-cover.${ext}`
}

function removeCoverImg(bookId: string) {
  COVER_IMG_EXTS.forEach(ext => {
    const coverPath = getCoverFilePath(bookId, ext)
    try {
      fs.unlinkSync(coverPath)
    } catch {
      // Catch file not exists
    }
  })
}

function setBookCoverImage(bookId: string) {
  const filePaths = dialog.showOpenDialogSync({
    title: 'Select book cover image',
    buttonLabel: 'Select',
    properties: ['openFile'],
    filters: [
      {name: 'Images', extensions: COVER_IMG_EXTS}
  ]})

  if (!filePaths) {
    return
  }

  const coverPathSrc = filePaths[0]

  const ext = path.extname(coverPathSrc).substring(1).toLowerCase()
  const coverPathDst = getCoverFilePath(bookId, ext)
  removeCoverImg(bookId)
  fs.copyFileSync(coverPathSrc, coverPathDst)
}

export function getBookCoverImage(bookId: string): string | null {
  for (const ext of COVER_IMG_EXTS) {
    try {
      const coverPath = getCoverFilePath(bookId, ext)
      return getImageDataUrl(coverPath)
    } catch {
      // Catch file not exists
    }
  }

  return null
}

export function initBookCoverIpc() {
  ipcMain.on('setBookCoverImageSync', (event, bookId) => {
    setBookCoverImage(bookId)
    event.returnValue = getBookCoverImage(bookId)
  })

  ipcMain.on('getBookCoverImageSync', (event, bookId) => {
    event.returnValue = getBookCoverImage(bookId)
  })
}
