import {fetchStoryBodies} from 'atun-core'

import {CancelRequested} from './cancel'
import {Article, loadArticles, selectArticles, storeArticles} from './db'
import {newPageMaker} from './pie'

export async function fetchMissingArticleBodies(
  bookId: string,
  idxs: number[],
  cancelRequested: CancelRequested,
  onFetchStart: (article: Article) => void,
  onFetchFinish: (article: Article) => void
): Promise<Article[]> {
  const articles = loadArticles(bookId)
  const selectedArticles = selectArticles(articles, idxs)
  const articlesMissingBodies = selectedArticles.filter(({body}) => !body)

  if (articlesMissingBodies.length === 0) {
    return selectedArticles
  }

  const pageMaker = newPageMaker()

  onFetchStart(articlesMissingBodies[0])

  for await (const fetchedBody of fetchStoryBodies(pageMaker, articlesMissingBodies, cancelRequested)) {
    const article = articlesMissingBodies[fetchedBody.idx]
    article.body = 'error' in fetchedBody ? '' : fetchedBody.body

    storeArticles(bookId, articles)
    onFetchFinish(article)

    const nextIdx = fetchedBody.idx + 1
    if (nextIdx < articlesMissingBodies.length) {
      onFetchStart(articlesMissingBodies[nextIdx])
    }
  }

  return selectedArticles
}
