// https://github.com/electron/electron/issues/9920
window.electron = {
  ipcRenderer: require('electron').ipcRenderer,
  clipboard: require('electron').clipboard
}
