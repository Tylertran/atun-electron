import {ipcMain} from 'electron'

// Key/value table of cancel token -> cancel requested (true/false)
const cancelRequests: {[token: string]: boolean} = {}

export type CancelRequested = () => boolean
export type CancelableTask = (cancelRequested: CancelRequested) => void

// Returns token, which can be used to cancel the task.
export function submitTask(cancelableTask: CancelableTask) {
  // This uniqueness is good enough
  const token = Date.now().toString()

  cancelRequests[token] = false
  const cancelRequested = () => cancelRequests[token]

  process.nextTick(async () => {
    try {
      await cancelableTask(cancelRequested)
    } catch (e) {
      console.log(e)
    } finally {
      delete cancelRequests[token]
    }
  })

  return token
}

export function requestCancel(token: string) {
  // This may be called after the task has already finished,
  // and "delete cancelRequests[token]" has been called
  if (cancelRequests[token] === false) cancelRequests[token] = true
}

export function initCancelIpc() {
  ipcMain.on('requestCancel', (event, token) =>
    requestCancel(token)
  )
}
