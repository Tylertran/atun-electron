// Users can mess with the files.
// Thus the code in this file should be defensive to prevent
// the app from crashing permanently.

import {loadJson, storeJson} from 'atun-core'
import * as fs from 'fs'

const DB = './tmp'
const BOOKS = `${DB}/books.json`

function getBookFilePath(id: string) {
  return `${DB}/book-${id}.json`
}

export type Book = {
  id: string
  url: string  // Facebook page URL
  title: string
}

export type Article = {
  url: string
  title: string
  date: string   // TODO Convert to Unix time
  body?: string  // May not exist if the body hasn't been collected
}

//------------------------------------------------------------------------------

export function storeBooks(books: Book[]) {
  storeJson(BOOKS, books)
}

export function loadBooks(): Book[] {
  const books = loadJson(BOOKS)
  return books || []
}

function getNewBookId() {
  return Date.now().toString()
}

// Returns book ID.
export function addBook(url: string, title: string): string {
  const books = loadBooks()

  const id = getNewBookId()
  books.unshift({id, url, title})

  storeBooks(books)
  return id
}

export function setBookTitle(id: string, title: string) {
  const books = loadBooks()
  const book = books.find(b => b.id === id)

  if (book) {
    book.title = title
    storeBooks(books)
  }
}

export function deleteBook(id: string) {
  const books = loadBooks()
  const idx = books.findIndex(b => b.id === id)

  if (idx >= 0) {
    books.splice(idx, 1)
    storeBooks(books)

    const bookFilePath = getBookFilePath(id)
    if (fs.existsSync(bookFilePath)) fs.unlinkSync(bookFilePath)
  }
}

//------------------------------------------------------------------------------

export function storeArticles(bookId: string, articles: Article[]) {
  const filePath = getBookFilePath(bookId)
  storeJson(filePath, articles)
}

export function loadArticles(bookId: string): Article[] {
  const filePath = getBookFilePath(bookId)
  try {
    return loadJson(filePath) || []
  } catch (e) {
    console.log(e)
    return []
  }
}
// To save memory, article bodies are not sent from Electron to browser.
// The browser side only knows whether the bodies have been fetched or not.
// `body` to `bodied` mapping:
// - undefined -> false (not fetched yet)
// - null      -> null (failed to fetch)
// - string    -> true (fetched successfully)
export function articleBodied(article: Article): boolean | null {
  const {body} = article
  return body === undefined
    ? false
    : body === null
      ? null
      : true
}

export function selectArticles(articles: Article[], idxs: number[]) {
  return idxs.map(idx => articles[idx])
}
