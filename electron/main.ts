import {app, BrowserWindow} from 'electron'
import isDev from 'electron-is-dev'
import path from 'path'

import {initBooksIpc} from './books'
import {initCancelIpc} from './cancel'
import {initBookCoverIpc} from './cover'
import {initExportIpc} from './export'
import {initFbViewIpc} from './fb'
import {initLinksIpc} from './links'
import {initPuppeteer} from './pie'

initPuppeteer()

const WIDTH = 1200
const HEIGHT = 800
const MIN_WIDTH = 900
const MIN_HEIGHT = 600

const DEV_URL = 'http://localhost:3000'
const PRD_URL = `file://${path.join(__dirname, '../build/index.html')}`

let mainWindow: undefined | BrowserWindow = undefined

function initMainWindow() {
  // Prepare Electron side first, then load the web site

  mainWindow = new BrowserWindow({
    width: WIDTH,
    height: HEIGHT,
    minWidth: MIN_WIDTH,
    minHeight: MIN_HEIGHT,

    webPreferences: {
      // Use `preload` instead of `nodeIntegration`:
      // https://www.electronjs.org/docs/tutorial/security#2-do-not-enable-nodejs-integration-for-remote-content
      //
      // When we package the app for production, preload.js will be put in archive file app.asar,
      // but Electron will still be able to load it:
      // https://www.electronjs.org/docs/tutorial/application-packaging
      preload: path.join(__dirname, 'preload.js')
    }
  })

  mainWindow.on('closed', () => (mainWindow = undefined))

  mainWindow.setMenuBarVisibility(isDev)

  initFbViewIpc(mainWindow)
  initBooksIpc()
  initBookCoverIpc()
  initLinksIpc()
  initExportIpc()
  initCancelIpc()

  mainWindow.loadURL(isDev ? DEV_URL : PRD_URL)

  if (isDev) {
    // Open Dev Tools in a separate window for simplicity; this may also be useful:
    // https://stackoverflow.com/questions/53678438/dev-tools-size-and-position-in-electron
    mainWindow.webContents.openDevTools({mode: 'detach'})
  }
}

app.on('ready', initMainWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (!mainWindow) {
    initMainWindow()
  }
})
