// Puppeteer doesn't support printing in non-headless mode,
// and Electron can't run in headless mode:
// https://github.com/puppeteer/puppeteer/blob/v5.2.1/docs/api.md#pagepdfoptions
//
// Electron supports printing:
// https://www.electronjs.org/docs/api/web-contents#contentsprintoptions-callback
// https://www.electronjs.org/docs/api/web-contents#contentsprinttopdfoptions
//
// But Electron printing doesn't support setting page numbers at each page footer.
// Workaround:
// - Print PDF without page numbers
// - Use pdf-lib to draw page numbers
export function print() {
  // window.webContents.print({header: 'header', footer: 'footer'})
  // page.pdf({path: './xxx.pdf'})
  // window.destroy();
}
