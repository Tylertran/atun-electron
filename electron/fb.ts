import {BrowserView, BrowserWindow, ipcMain} from 'electron'

export const FB_BASE_URL = 'https://www.facebook.com'

const TOP_BAR_HEIGHT = 108
const FB_TAB_IDX = 0

let onLine = false
let tabIdx = -1

function setViewAndAdjustSize(mainWindow: BrowserWindow, fbView: BrowserView) {
  // Must call this first before setting the size
  mainWindow.setBrowserView(fbView)

  const [width, height] = mainWindow.getContentSize()
  fbView.setBounds({
    x: 0,
    y: TOP_BAR_HEIGHT,
    width: width,
    height: height - TOP_BAR_HEIGHT
  })
  fbView.setAutoResize({width: true, height: true})
}

function createFbView() {
  const fbView = new BrowserView()
  const fwc = fbView.webContents

  // Prevent users from opening new windows (making a mess)
  fwc.on('new-window', (event) => {
    console.log('new-window', event)
    event.preventDefault()
  })

  // Prevent navigating to outside of Facebook
  fwc.on('will-navigate', (event, url) => {
    console.log('will-navigate', url)
    if (!url.startsWith(FB_BASE_URL)) {
      event.preventDefault()
    }
  })

  // updateFbViewDisplay will be called later

  return fbView
}

function updateFbViewDisplay(mainWindow: BrowserWindow, fbView: BrowserView) {
  if (onLine && tabIdx === FB_TAB_IDX) {
    // User may have resized mainWindow while FB view was removed (hidden)
    setViewAndAdjustSize(mainWindow, fbView)
  } else {
    mainWindow.removeBrowserView(fbView)
  }
}

export function initFbViewIpc(mainWindow: BrowserWindow) {
  const fbView = createFbView()

  const mwc = mainWindow.webContents
  const fwc = fbView.webContents

  ipcMain.on('onLineChange', (event, isOnLine) => {
    onLine = isOnLine
    updateFbViewDisplay(mainWindow, fbView)
    if (onLine) fwc.loadURL(FB_BASE_URL)
  })

  fwc.on('did-navigate', (event, url) => {
    mwc.send('fbUrlChange', url)
  })

  fwc.on('did-navigate-in-page', (event, url) => {
    mwc.send('fbUrlChange', url)
  })

  ipcMain.on('getFbUrlSync', (event) => {
    event.returnValue = fwc.getURL()
  })

  ipcMain.on('getFbTitleSync', (event) => {
    event.returnValue = fwc.getTitle()
  })

  ipcMain.on('fbGoBack', () => {
    fwc.goBack()
  })

  ipcMain.on('fbGoForward', () => {
    fwc.goForward()
  })

  ipcMain.on('fbGoHome', () => {
    fwc.loadURL(FB_BASE_URL)
  })

  ipcMain.on('tabChange', (event, tabIndex) => {
    tabIdx = tabIndex
    updateFbViewDisplay(mainWindow, fbView)
  })
}
