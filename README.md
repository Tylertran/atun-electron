# Atun desktop app

This Electron app is based on:
[create-react-app-electron-typescript-boilerplate](https://github.com/ngocdaothanh/create-react-app-electron-typescript-boilerplate)

## Run app in dev mode

```bash
npm run react-start
npm run electron-start
```

To start both:

```bash
npm start
```

## Build production app

```bash
npm run react-build
npm run electron-bundle
npm run electron-pack
```

The production app will be put in `dist` directory.

## Dev notes

For simplicity, we don't use a proper DB like
[SQLite](https://github.com/mapbox/node-sqlite3).
When generating PDF/HTML book, we have to load all the book data to memory anyway.

SQLite may not support latest versions of Electron:
* https://github.com/mapbox/node-sqlite3/issues/1337
* https://github.com/mapbox/node-sqlite3/issues/1331

Files to persist books:
* `books.json`: list of books; this is the entrypoint to walk through the books
* `book-<book creation time in ms>.json`: articles in the book
* `book-<book creation time in ms>-cover.png etc.`: book cover image

## TODO

Investigate if when building for production:
* Need to change `"module": "CommonJS"` in tsconfig.electron.json to `"module": "esnext"`?
* Make sure electron/preload.js can be loaded in production.
